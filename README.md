# Clean benchtop

This design is part of the Ground Support Equipment used inside the Libre Space Foundation and is designed to be modular and easy to setup where needed. It can be easily upscaled if needed, in order to incorporate bigger devices inside. 
